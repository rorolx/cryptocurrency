import React from 'react'
import './styles.css'

const Currency = props => {
   
    const data = props.currency.results
        // console.log(data); 
 
        const currencies = [] 

        // loop
        for( let obj in data ){ 
            currencies.push( data[obj] ) 
        }  

        const uniqueCurrencyId = Array.from(new Set(currencies.map(a => a.currencyId)))
        .map(id => {
            return currencies.find(a => a.currencyId === id)
        })
              
            // console.log( uniqueCurrencyId.map( key => key.currencyId) )
   
    return(
        <div className="container-select"> 
            <label htmlFor={props.label}>{props.label}</label>
            <select 
                    className="__select"
                    name={props.name}
                    target={props.target}
                    onChange={(e) => props.selectChange(e, props.name)} 
            >
                {
                    uniqueCurrencyId.map( key => <option value={key.currencyId} key={key.id}>{key.currencyId} - {key.currencyName}</option>) 
                }
            </select>
        </div>
    )
}

export default Currency

// https://dev.to/vuevixens/removing-duplicates-in-an-array-of-objects-in-js-with-sets-3fep

// for (const result of results) console.log(result)

// or

// for (const {data, links, href} of results) …

// or

// results.forEach(({data, links, href) =>