import React from 'react'
import './styles.css'


const Convert = props => { 

    const data = props.convert
    const reference = props.reference
    const amount = props.amount 
    const total = (data[reference] * amount)
    
// console.log('data:: ', data)
// console.log('reference:: ', reference)
// console.log('amount:: ', amount)

    const getAmount = () =>{ 
        if( 
            amount !== 0 && 
            !new RegExp(/['null']/).test(reference) &&
            data[reference] !== undefined
        ){
                    if( !isNaN(total) ) return total.toFixed(2)
           
        }
    }
     
    return(
        <div className="container-convertion">  
                <span>
                    {
                       getAmount()
                    }
                </span>
        </div>
    )
       
}

export default Convert

/* 
 API

    CURRCONV
    https://free.currconv.com/api/v7/convert?q=USD_PHP&compact=ultra&apiKey=01cac8d7ca54a81ab198
    country
    https://free.currconv.com/api/v7/countries?apiKey=01cac8d7ca54a81ab198

    -----------------------------------------

    FIXER
    https://fixer.io/quickstart
    API Key : e73371f9f015cd7c182692b257027a16
    http://data.fixer.io/api/latest?access_key=e73371f9f015cd7c182692b257027a16
    
    http://data.fixer.io/api/convert?access_key=YOUR_ACCESS_KEY&from=USD&to=EUR&amount=25

*/