import React from 'react'
import './styles.css'

const Input = props => { 
    return(
        <div className="container-input">
            <label>{props.label}</label>
            <input
                className='__input' 
                type={props.type} 
                value={props.value}
                // placeholder={props.amount}
                onChange={props.handleChange} 
            />
        </div>
    ) 
}

export default Input