import React from 'react'
import './styles.css'

const Button = props => {
    return(
        <button 
            className="__button"
            onClick={props.handleClick}
        >
        {props.text}
        </button> 
    )
}

export default Button