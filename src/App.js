import React from 'react';
import Currency from './components/currency'
import Input from './components/input'
import Button from './components/button'
import Convertion from './components/convertion'


import './App.css'

class App extends React.Component {

  state = {
    currency:[],
    from: undefined, 
    to:undefined,
    amount:1,
    convert:[]
  }

  componentDidMount(){
    fetch('./currency.json')
    .then( response => response.json())
    .then( json => {this.setState({ currency : json }); console.log(json)})
   
  }

  selectChange = (e, name, symbol) => { 
      this.setState({
        [name]: e.target.value,
      }) 
  }

  handleChange = e => { 
      this.setState({ amount : e.target.value })
  }

  handleClick = e => {
    e.preventDefault() 
    this.getConvertion() 
  }

  getConvertion = () => {

    const{ from, to }= this.state
    fetch(`https://free.currconv.com/api/v7/convert?q=${from}_${to}&compact=ultra&apiKey=01cac8d7ca54a81ab198`)
    .then(response => response.json())
    .then ( json => this.setState({ convert: json }))
    // console.log(this.state.convert)
  }

  render(){

    // console.log(this.state.from,' - ',  this.state.to)
      const { currency, from, to, amount, convert } = this.state

      return (
      <div className="App">
        <h1>Currencies converter</h1>
        <div className="container-form">
            <form>
                <Currency 
                        currency={currency} 
                        label="from" 
                        name="from" 
                        selectChange={this.selectChange}
                />
                <Currency 
                        currency={currency} 
                        label="to" 
                        name="to"
                        selectChange={this.selectChange}
                />
               <Input 
                    type="number"
                    label="amount" 
                    value={amount}
                    // placeholder={amount}
                    handleChange={this.handleChange}
                />
               <Button 
                    text="Convert"
                    handleClick={this.handleClick}
               />
            </form>
        </div> 
        <Convertion 
            convert={convert} 
            amount={amount}
            reference={`${from}_${to}`} 
        /> 
      </div>
    );
  } 
}

export default App;